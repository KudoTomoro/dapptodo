pragma solidity ^0.5.0;

contract Todo{
  event newTask(uint id, string description, bool isCompleted);
 // ↑フロントエンドにタスクのid、内容、完了しているかどうかを返す

  struct Task{
    // タスクの内容
    string description;
    // タスクが完了してるかしてないか
    bool isCompleted;

  }

  Task[] public tasks;
  // タスクのIDに対して、アドレスを紐付け
  mapping (uint => address) taskToOwner;
  // 登録アドレスに対する、登録タスクの数を紐付け
  mapping (address => uint) ownerTaskCount;

  function saveTask(string memory _description) public {
    // タスクを登録 -1は配列番号を0から始めるためにつける
    uint id = tasks.push(Task(_description, false)) - 1;
    // 今コントラクトを管理しているアドレス情報がmsg.senderに入る
    // タスク番号とアドレスが紐付けられる
    taskToOwner[id] = msg.sender;
    // 登録アドレスに対する、登録タスクの数を紐付け
    ownerTaskCount[msg.sender]++;

    // フロントエンドにイベントを返す
    emit newTask(id, _description, false);
  }

  function getTaskIds(address _owner) external view returns (uint[] memory){
    // external・・・外部からのみ呼び出される
    // view・・・ブロックチェーン変更しないよ　ガスかからない

    // ガスがかからないようにmemory
    // taskのidを返すための配列
    uint[] memory result = new uint[](ownerTaskCount[_owner]);

    uint counter = 0;
    for (uint i=0; i<tasks.length; i++){
      if(taskToOwner[i] == _owner){
        result[counter] = i;
        // オーナーアドレスと一致するiの時resultに入れると、タスクidが返る
        counter++;
      }
    }

    return result;
  }
  // isCompletedを更新する関数
  function completeTask(uint _taskId) public{
    // コントラクトを呼び出すアドレスと、タスクを作成したアドレスが一致した時動かす
    require(msg.sender == taskToOwner[_taskId]);
    tasks[_taskId].isCompleted = true;
  }

 }
